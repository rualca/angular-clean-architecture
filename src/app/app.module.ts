import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { DataModule } from './data/data.module';
import { CoreModule } from './core/core.module';
import { CarRepository } from './core/repositories/car.repository';
import { PresentationModule } from './presentation/presentation.module';
import { CarMockRepository } from './data/repository/car-mock-repository/car-mock.repository';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    DataModule,
    CoreModule,
    PresentationModule
  ],
  providers: [
    {provide: CarRepository, useClass: CarMockRepository}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
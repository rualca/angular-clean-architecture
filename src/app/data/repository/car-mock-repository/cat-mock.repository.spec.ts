import { inject, TestBed } from '@angular/core/testing';

import { CarMockRepository } from './car-mock.repository';

describe('CarRepositoryMockService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CarMockRepository]
    });
  });

  it('should be created', inject([CarMockRepository], (service: CarMockRepository) => {
    expect(service).toBeTruthy();
  }));
});
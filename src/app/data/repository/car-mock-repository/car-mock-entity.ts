export interface CarMockEntity {
  id: number;
  manufacturer: string;
  model: string;
  year: number;
}
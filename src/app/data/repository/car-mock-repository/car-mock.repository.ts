import { Injectable } from '@angular/core';
import { CarRepository } from '../../../core/repositories/car.repository';
import { CarModel } from '../../../core/domain/car.model';
import { from, Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CarMockEntity } from './car-mock-entity';
import { CarMockRepositoryMapper } from './car-mock-repository.mapper';

@Injectable({
  providedIn: 'root'
})
export class CarMockRepository extends CarRepository {

  private mapper = new CarMockRepositoryMapper();

  cars = [
    {
      'id': 1,
      'manufacturer': 'Ford',
      'model': 'Kuga',
      'year': new Date().getFullYear()
    },
    {
      'id': 2,
      'manufacturer': 'Seat',
      'model': 'Ibiza',
      'year': new Date().getFullYear()
    },
    {
      'id': 3,
      'manufacturer': 'Renault',
      'model': 'Megane',
      'year': new Date().getFullYear()
    }
  ];

  constructor() {
    super();
  }

  getCarById(id: number): Observable<CarModel> {
    return from(this.cars)
      .pipe(filter((car: CarMockEntity) => car.id === id))
      .pipe(map(this.mapper.mapFrom));
  }

  getAllCars(): Observable<CarModel> {
    return from(this.cars)
      .pipe(map(this.mapper.mapFrom));
  }
}
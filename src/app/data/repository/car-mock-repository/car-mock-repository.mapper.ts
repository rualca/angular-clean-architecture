import { CarModel } from '../../../core/domain/car.model';
import { Mapper } from '../../../core/base/mapper';
import { CarMockEntity } from './car-mock-entity';

export class CarMockRepositoryMapper extends Mapper <CarMockEntity, CarModel> {
  mapFrom(param: CarMockEntity): CarModel {
    return {
      manufacturer: param.manufacturer,
      model: param.model,
      year: param.year
    };
  }

  mapTo(param: CarModel): CarMockEntity {
    return {
      id: 0,
      manufacturer: param.manufacturer,
      model: param.model,
      year: param.year
    };
  }
}
import { Injectable } from '@angular/core';
import { CarRepository } from '../../../core/repositories/car.repository';
import { CarModel } from '../../../core/domain/car.model';
import { Observable } from 'rxjs';
import { CarWebRepositoryMapper } from './car-web-repository.mapper';
import { HttpClient } from '@angular/common/http';
import { CarWebEntity } from './car-web.entity';
import { flatMap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CarWebRepository extends CarRepository {
  private mockAPIServer = 'http://5b8d40db7366ab0014a29bfa.mockapi.io/api/v1/cars'

  mapper = new CarWebRepositoryMapper();

  constructor(
    private http: HttpClient
  ) {
    super();
  }

  getCarById(id: number): Observable<CarModel> {
    return this.http
      .get<CarWebEntity>(`${this.mockAPIServer}/${id}`)
      .pipe(map(this.mapper.mapFrom));
  }

  getAllCars(): Observable<CarModel> {
    return this.http
      .get<CarWebEntity[]>(`${this.mockAPIServer}`)
      .pipe(flatMap((item) => item))
      .pipe(map(this.mapper.mapFrom));
  }
}
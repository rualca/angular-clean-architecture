import { CarWebEntity } from './car-web.entity';
import { CarModel } from '../../../core/domain/car.model';
import { Mapper } from '../../../core/base/mapper';

export class CarWebRepositoryMapper extends Mapper <CarWebEntity, CarModel> {
  mapFrom(param: CarWebEntity): CarModel {
    return {
      manufacturer: param.manufacturer,
      model: param.model,
      year: param.year
    };
  }

  mapTo(param: CarModel): CarWebEntity {
    return {
      id: 0,
      manufacturer: param.manufacturer,
      model: param.model,
      year: param.year
    };
  }
}
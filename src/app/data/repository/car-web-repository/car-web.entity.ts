export interface CarWebEntity {
  id: number;
  manufacturer: string;
  model: string;
  year: number;
}
import { Observable } from 'rxjs';
import { CarModel } from '../domain/car.model';

export abstract class CarRepository {
  abstract getCarById(id: number): Observable<CarModel>;
  abstract getAllCars(): Observable<CarModel>;
}
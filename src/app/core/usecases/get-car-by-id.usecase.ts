import { Injectable } from '@angular/core';
import { CarRepository } from '../repositories/car.repository';
import { UseCase } from '../base/use-case';
import { CarModel } from '../domain/car.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GetAllCarsUsecase implements UseCase<number, CarModel> {

  constructor(private carRepository: CarRepository) { }

  execute(params: number): Observable<CarModel> {
    return this.carRepository.getCarById(params);
  }
}
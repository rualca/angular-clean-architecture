import { Injectable } from '@angular/core';
import { CarRepository } from '../repositories/car.repository';
import { UseCase } from '../base/use-case';
import { CarModel } from '../domain/car.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GetAllCarsUsecase implements UseCase<void, CarModel> {

  constructor(private carRepository: CarRepository) { }

  execute(params: void): Observable<CarModel> {
    return this.carRepository.getAllCars();
  }
}
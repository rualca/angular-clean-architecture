export interface CarModel {
  manufacturer: string;
  model: string;
  year: number;
}
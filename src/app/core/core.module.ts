import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/*
Why there is an Angular annotation on a core layer where theoretically should
only be plain Typescript without any external dependencies to frameworks?

The reason is, that Angular only has this @Injectable annotation to provide a
module via dependency injection. Exists a fourth layer in clean code named
configuration. If Angular would have such a functionality like Spring Boot
or Dagger does, then the configuration layer could take care of our dependency
injection. But for now, we have to stick to this solution as long as we do not
want to hack the dependency injection mechanism
*/

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [

  ],
  exports: [

  ]
})
export class CoreModule { }
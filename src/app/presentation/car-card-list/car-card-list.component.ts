import { Component } from '@angular/core';
import { CarModel } from '../../core/domain/car.model';
import { GetAllCarsUsecase } from '../../core/usecases/get-all-cars.usecase';

@Component({
  selector: 'app-car-card',
  templateUrl: './car-card-list.component.html',
  styleUrls: []
})
export class CarCardListComponent {

  cars: Array<CarModel> = [];

  constructor(private getAllCars: GetAllCarsUsecase) { }

  updateCars() {
    this.cars = [];

    this.getAllCars.execute().subscribe((value: CarModel) =>
      this.cars.push(value)
    );
  }

  onSelect(event: { target: any; }) {
    console.log(event.target);
  }
}
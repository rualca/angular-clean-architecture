import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarCardListComponent } from './car-card-list/car-card-list.component';
import { CoreModule } from '../core/core.module';
import { DataModule } from '../data/data.module';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    DataModule
  ],
  declarations: [
    CarCardListComponent
  ],
  exports: [
    CarCardListComponent
  ],
  providers: [
  ]
})
export class PresentationModule { }